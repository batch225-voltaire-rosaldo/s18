// Function Parameters and Arguments

// "name" is called a parameter that acts as a named variable or container that exists only inside of the function

// "Juana" - argument - it is the information/data provided directly into the function

function printInput(name) {
	console.log("My name is " + name);
}

// printInput("Juana");
// printInput("Adam");
// printInput("Allan");

let sampleVariable = "Yuri";
// let sampleVar2 = prompt("Your name:");
printInput(sampleVariable);

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Functions as Arguments

function argumentFunction(name) {
	console.log("This function was passed as an argument before the msg was printed." + name);
}

function argumentFunction2() {
	console.log("This function was passed as an argument2 before the msg was printed.");
}

function invokeFunction(argumentFunction) {
	argumentFunction(name);
	console.log("This code comes from invokeFunction");
}

invokeFunction(argumentFunction(Name));






